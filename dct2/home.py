import numpy as np

# Implementation of DTC as:
# c[k] = 1/a[k] * sum(cos(k*pi*(2*i-1)/(2*n))*v[i])
def dct_home(vector):
    # Length of vector v
    n = vector.shape[0]
    # k vector of indexes as [0, n-1]
    k = np.arange(n)
    # Compute the argument of the cosine function
    # c = 2 * c + 1     - (2*i+1)
    # c = c / (2 * n)   - (2*i+1)/(2*n)
    # c = np.pi * c     - pi*(2*i+1)/(2*n)
    # c = k * c         - k*pi*(2*i+1)/(2*n)
    c = k[:, None] * np.pi * (2*k+1)/(2*n)
    # Compute the cosine function element-wise
    c = np.cos(c)     # cos(k*pi*(2*i+1)/(2*n))
    # Compute the product with vector v
    c = c * vector      # cos(k*pi*(2*i+1)/(2*n))*v[i]
    # Compute the sum of each row
    c = np.sum(c, axis=1)  # sum(cos(k*pi*(2*i+1)/(2*n))*v[i])
    # Compute the normalization factors
    a = np.ones(n) * np.sqrt(2/n)
    a[0] = np.sqrt(1/n)
    # Multiply by the norm factor
    c = a * c       # 1/a[k] * sum(cos(k*pi*(2*i-1)/(2*n))*v[i])
    return c


# DCT2
def dct2_home(matrix):
    # Matrix shape
    n, m = matrix.shape

    # k, l vector of indexes as [0, n-1]
    k = np.arange(n)
    l = np.arange(m)

    # Compute the argument of the cosine function
    ci = k[:, None] * np.pi * (2*k+1)/(2*n)
    cj = l[:, None] * np.pi * (2*l+1)/(2*m)

    # Compute the cosine function element-wise
    ci = np.cos(ci)
    cj = np.cos(cj)

    # c matrix of DCT2 coefficients
    c = np.zeros((n, m))
    # Compute c coefficients
    for i in k :
        for j in l :
            # Compute sub-matrix
            t = ci[i, :, None] * cj[j, :]
            # Compute the product with input matrix
            t = t * matrix
            # Compute the sum of each sub-matrix
            c[i, j]= np.sum(np.sum(t))

    # Compute the normalization factors
    ai = np.ones(n) * np.sqrt(2/n)
    ai[0] = np.sqrt(1/n)
    aj = np.ones(m) * np.sqrt(2/m)
    aj[0] = np.sqrt(1/m)
    a = ai[:, None] * aj

    # Multiply by the norm factor
    c = a * c

    return c
