from scipy.fftpack import dctn, idctn

def dct2_scipy(matrix):
    return dctn(matrix, norm='ortho')

def idct2_scipy(matrix):
    return idctn(matrix, norm='ortho')
