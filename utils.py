#!/usr/bin/env python
import os
import dct2
import matplotlib.pyplot as plt
from matplotlib.image import imread


def paths_from_folder(path = './data'):
    paths = [
        os.path.join(directory, file)
        for (directory, _, files) in os.walk(path)
        for file in files
        if file.endswith('.bmp')
    ]
    return paths

def load_image_from_path(path):
    return imread(path)

if __name__ == '__main__':
    images = paths_from_folder()
    images = [
        load_image_from_path(image)
        for image in images
    ]
