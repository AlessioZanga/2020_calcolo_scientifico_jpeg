import tkinter as tk
import numpy as np
from os.path import isfile
from PIL import Image
from PIL.ImageQt import ImageQt
from tkinter import filedialog
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import pyqtSlot, Qt
from PyQt5.QtWidgets import QMainWindow
from PyQt5.uic import loadUi
from multiprocessing import Pool, cpu_count
from .utils import split_matrix, reassemble_matrix, delete_frequencies, adjust_values, to_jpeg


class MainView(QMainWindow):

    _img_matrix: np.array
    
    def __init__(self):
        super(MainView, self).__init__()
        loadUi("gui/main_gui.ui", self)
        self.setWindowTitle("Jpeg Compression")

        # Declare widgets and methods
        self.button_load_image.clicked.connect(self.load_image_clicked)
        self.button_to_jpeg.clicked.connect(self.to_jpeg_clicked)

        # Disable to_jpeg button until image is selected
        self.button_to_jpeg.setEnabled(False)

        self.slider_F.valueChanged.connect(self.slider_F_valueChanged)
        self.slider_d.valueChanged.connect(self.slider_d_valueChanged)

    @pyqtSlot()
    def load_image_clicked(self):
        tk.Tk().withdraw()
        path = filedialog.askopenfilename()
        # Update iff the path is valid
        if isfile(str(path)):
            # Open image and convert to B/W scale
            bit_img = Image.open(path).convert("L")
            # Create matrix from B/W image
            self._img_matrix = np.array(bit_img)
            # Show (original) image on window
            pixmap = QPixmap.fromImage(ImageQt(bit_img))
            self.label_show_orig_img.setPixmap(pixmap.scaled(self.label_show_orig_img.size(), Qt.KeepAspectRatio))
            # Update slider_F upper value with minumum value of width/height image
            self.set_slider_F_upper_label(min(self._img_matrix.shape))
            # Enable to_jpeg button
            self.button_to_jpeg.setEnabled(True)

    @pyqtSlot()
    def to_jpeg_clicked(self):
        F = self.slider_F.value()
        d = self.slider_d.value()
        rows = int(self._img_matrix.shape[0] / F)
        cols = int(self._img_matrix.shape[1] / F)
        macro_blocks = split_matrix(self._img_matrix, F)
        
        pool = Pool(cpu_count())
        macro_blocks = [
            (macro_block, F, d)
            for macro_block in macro_blocks
        ]
        macro_blocks_jpeg = pool.starmap(to_jpeg, macro_blocks)
        pool.close()
        pool.join()
        
        jpeg_matrix = reassemble_matrix(macro_blocks_jpeg, rows, cols)        
        # Show (original) image on window
        pixmap = QPixmap.fromImage(ImageQt(Image.fromarray(jpeg_matrix).convert("L")))
        self.label_show_comp_img.setPixmap(pixmap.scaled(self.label_show_orig_img.size(), Qt.KeepAspectRatio))
        

    # Slider_F methods
    @pyqtSlot()
    def slider_F_valueChanged(self):
        F = self.slider_F.value()
        self.label_slider_F_value.setText(str(F))
        # Update slider_d upper value with 2F-2
        self.set_slider_d_upper_value(F)

    def set_slider_F_upper_label(self, value):
        self.slider_F.setMaximum(value)
        self.label_upper_F_slider.setText(str(value))

    # Slider_d methods
    @pyqtSlot()
    def slider_d_valueChanged(self):
        self.label_slider_d_value.setText(str(self.slider_d.value()))
    
    def set_slider_d_upper_value(self, F):
        value = (F * 2) - 2
        self.slider_d.setMaximum(value)
        self.label_upper_d_slider.setText(str(value))
