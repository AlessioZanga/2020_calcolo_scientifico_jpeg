import numpy as np
from dct2 import dct2_scipy, idct2_scipy


def split_matrix(matrix, F):
    rows, cols = matrix.shape
    matrix = matrix[0:(rows-rows % F), 0:(cols-cols % F)]
    matrix = matrix.reshape(rows // F, F, -1, F)
    matrix = matrix.swapaxes(1, 2)
    matrix = matrix.reshape(-1, F, F)
    return matrix


def reassemble_matrix(macro_blocks, rows, cols):
    F, _ = macro_blocks[0].shape
    macro_blocks = np.array(macro_blocks)
    macro_blocks = macro_blocks.reshape(rows * F // F, -1, F, F)
    macro_blocks = macro_blocks.swapaxes(1, 2)
    macro_blocks = macro_blocks.reshape(rows * F, cols * F)
    return macro_blocks


def delete_frequencies(matrix, F, d):
    # Matrix has shape FxF
    for i in range(F):
        for j in range(F):
            if i + j >= d:
                matrix[i, j] = 0
    return matrix

# Negative values -> 0   Values greater then 255 -> 255


def adjust_values(matrix):
    return np.clip(matrix, 0, 255)


def to_jpeg(macro_block, F, d):
    c = dct2_scipy(macro_block)
    c = delete_frequencies(c, F, d)
    ff = idct2_scipy(c)
    ff = adjust_values(ff)
    return ff
