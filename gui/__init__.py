from .gui import MainView
from .utils import (split_matrix, 
                    reassemble_matrix, 
                    delete_frequencies, 
                    adjust_values
                    )