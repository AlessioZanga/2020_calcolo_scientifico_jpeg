#!/usr/bin/env python
import sys
from PyQt5.QtWidgets import QApplication
from gui import MainView


if __name__ == '__main__':

    app = QApplication(sys.argv)
    w = MainView()
    w.show()

    sys.exit(app.exec_())
    